package com.sdr.warehouse.service;

import com.sdr.warehouse.dto.TransactionProductDto;
import com.sdr.warehouse.entity.Client;
import com.sdr.warehouse.entity.Transaction;
import com.sdr.warehouse.entity.TransactionProduct;
import com.sdr.warehouse.exception.NotEnoughItemsInWarehouseException;
import com.sdr.warehouse.repository.ClientRepository;
import com.sdr.warehouse.repository.TransactionRepository;
import com.sdr.warehouse.repository.WarehouseRepository;
import com.sdr.warehouse.validator.TransactionProductValidator;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Transactional
    @Synchronized
    public Transaction createTransaction(Long clientId, List<TransactionProductDto> transactionProductDtos) throws NotEnoughItemsInWarehouseException {
        var client = clientRepository.findById(clientId).get(); //powinno być sprawdzenie czy Optional nie jest pusty
        var transaction = Transaction.of(client);
        var products = mapProductDtosToEntities(transactionProductDtos, transaction);

        products.forEach(TransactionProductValidator::validate);
        transaction.setProducts(products);
        transactionRepository.save(transaction);
        products.forEach(this::updateProductQuantityInWarehouse);

        return transaction;
    }

    private List<TransactionProduct> mapProductDtosToEntities(List<TransactionProductDto> transactionProductDtos, Transaction transaction) {
        return transactionProductDtos.stream()
                .map(dto -> TransactionProduct.of(transaction,
                        warehouseRepository.findById(dto.getWarehouseProductId()).get(), dto.getNumberOfItems())) //powinno być sprawdzenie czy Optional nie jest pusty
                .collect(Collectors.toList());
    }

    private void updateProductQuantityInWarehouse(TransactionProduct transactionProduct) {
        var warehouseProduct = transactionProduct.getWarehouseProduct();
        warehouseProduct.setNumberOfItems(warehouseProduct.getNumberOfItems() - transactionProduct.getNumberOfItems());
        warehouseRepository.save(warehouseProduct);
    }

    public List<Transaction> getTransactionHistoryForClient(Long clientId) {
        return transactionRepository.findByClient(Client.of(clientId));
    }

    public List<Transaction> getTransactionHistoryForProduct(Long productId) {
        return transactionRepository.findByProduct(productId);
    }
}
