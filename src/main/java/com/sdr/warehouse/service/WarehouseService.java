package com.sdr.warehouse.service;

import com.sdr.warehouse.entity.WarehouseProduct;
import com.sdr.warehouse.exception.ProductNotFoundException;
import com.sdr.warehouse.repository.ProductRepository;
import com.sdr.warehouse.repository.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WarehouseService {

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private ProductRepository productRepository;

    public WarehouseProduct addWarehouseItem(Long productId, Integer numberOfItems) throws ProductNotFoundException {
        var optionalWarehouseProduct = warehouseRepository.findByProductId(productId);

        //Jeżeli produkt jest już w magazynie to zwiększamy jego liczbę sztuk, jeżeli nie ma to dodajemy nowy wpis
        if (optionalWarehouseProduct.isPresent()) {
            return increaseQuantityOfProduct(optionalWarehouseProduct.get(), numberOfItems);
        } else {
            return addNewProductToWarehouse(productId, numberOfItems);
        }
    }

    private WarehouseProduct addNewProductToWarehouse(Long productId, Integer numberOfItems) throws ProductNotFoundException {
        var optionalProduct = productRepository.findById(productId);
        if (optionalProduct.isPresent()) {
            return warehouseRepository.save(WarehouseProduct.of(optionalProduct.get(), numberOfItems));
        } else {
            throw new ProductNotFoundException();
        }
    }

    private WarehouseProduct increaseQuantityOfProduct(WarehouseProduct warehouseProduct, Integer numberOfItems) {
        warehouseProduct.setNumberOfItems(warehouseProduct.getNumberOfItems() + numberOfItems);
        return warehouseRepository.save(warehouseProduct);
    }

    public Iterable<WarehouseProduct> getAllWarehouseProducts() {
        return warehouseRepository.findAll();
    }
}
