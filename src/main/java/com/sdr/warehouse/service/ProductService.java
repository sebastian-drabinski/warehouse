package com.sdr.warehouse.service;

import com.sdr.warehouse.entity.Product;
import com.sdr.warehouse.exception.ProductAlreadyExistsException;
import com.sdr.warehouse.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Product addProduct(Product product) throws ProductAlreadyExistsException {
        if (productRepository.findByName(product.getName()).isPresent()) {
            throw new ProductAlreadyExistsException();
        }
        return productRepository.save(product);
    }

    public Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }
}
