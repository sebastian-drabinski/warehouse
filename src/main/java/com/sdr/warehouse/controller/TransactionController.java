package com.sdr.warehouse.controller;

import com.sdr.warehouse.dto.TransactionProductDto;
import com.sdr.warehouse.entity.Transaction;
import com.sdr.warehouse.exception.NotEnoughItemsInWarehouseException;
import com.sdr.warehouse.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("transactions")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @PostMapping("/create")
    public Transaction create(Long clientId, @RequestBody List<TransactionProductDto> transactionProducts) {
        try {
            return transactionService.createTransaction(clientId, transactionProducts);
        } catch (NotEnoughItemsInWarehouseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Not enough items in warehouse for product with id=%d", e.getProductId()), e);
        }
    }

    @GetMapping("historyForClient")
    public List<Transaction> getHistoryForClient(Long clientId) {
        return transactionService.getTransactionHistoryForClient(clientId);
    }

    @GetMapping("historyForProduct")
    public List<Transaction> getHistoryForProduct(Long productId) {
        return transactionService.getTransactionHistoryForProduct(productId);
    }
}
