package com.sdr.warehouse.controller;

import com.sdr.warehouse.entity.Client;
import com.sdr.warehouse.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("clients")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping("/add")
    public Client add(@RequestBody Client client) { //jako argument metody zamiast encji powinno być dto
        return clientService.addClient(client);
    }

    @GetMapping("/all")
    public Iterable<Client> all() {
        return clientService.getAllClients();
    }
}
