package com.sdr.warehouse.controller;

import com.sdr.warehouse.entity.WarehouseProduct;
import com.sdr.warehouse.exception.ProductNotFoundException;
import com.sdr.warehouse.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("warehouse")
public class WarehouseController {

    @Autowired
    private WarehouseService warehouseService;

    @PostMapping("/add")
    public WarehouseProduct add(Long productId, Integer numberOfItems) {
        try {
            return warehouseService.addWarehouseItem(productId, numberOfItems);
        } catch (ProductNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product with given ID not found in database", e);
        }
    }

    @GetMapping("/all")
    public Iterable<WarehouseProduct> all() {
        return warehouseService.getAllWarehouseProducts();
    }
}
