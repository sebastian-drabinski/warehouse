package com.sdr.warehouse.controller;

import com.sdr.warehouse.entity.Product;
import com.sdr.warehouse.exception.InvalidProductPriceException;
import com.sdr.warehouse.exception.ProductAlreadyExistsException;
import com.sdr.warehouse.service.ProductService;
import com.sdr.warehouse.validator.ProductValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/add")
    public Product add(@RequestBody Product product) { //jako argument metody zamiast encji powinno być dto
        try {
            ProductValidator.validate(product);
            return productService.addProduct(product);
        } catch (InvalidProductPriceException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid product price", e);
        } catch (ProductAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Product already exists", e);
        }
    }

    @GetMapping("/all")
    public Iterable<Product> all() {
        return productService.getAllProducts();
    }
}
