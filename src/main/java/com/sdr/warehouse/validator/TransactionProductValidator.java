package com.sdr.warehouse.validator;

import com.sdr.warehouse.entity.TransactionProduct;
import com.sdr.warehouse.exception.NotEnoughItemsInWarehouseException;

public class TransactionProductValidator {

    private TransactionProductValidator() {
    }

    public static void validate(TransactionProduct product) throws NotEnoughItemsInWarehouseException {
        validateWarehouseQuantity(product);
    }

    private static void validateWarehouseQuantity(TransactionProduct transactionProduct) throws NotEnoughItemsInWarehouseException {
        var warehouseProduct = transactionProduct.getWarehouseProduct();
        //Porównanie liczby sztuk produktu zamówionej w transakcji z liczbą sztuk produktu dostępną w magazynie
        if (transactionProduct.getNumberOfItems() > warehouseProduct.getNumberOfItems()) {
            throw new NotEnoughItemsInWarehouseException(warehouseProduct.getProduct().getId());
        }
    }
}
