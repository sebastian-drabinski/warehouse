package com.sdr.warehouse.validator;

import com.sdr.warehouse.entity.Product;
import com.sdr.warehouse.exception.InvalidProductPriceException;

public class ProductValidator {

    private ProductValidator() {
    }

    public static void validate(Product product) throws InvalidProductPriceException {
        validatePrice(product);
    }

    private static void validatePrice(Product product) throws InvalidProductPriceException {
        if (product.getPrice() == null || product.getPrice() < 0) {
            throw new InvalidProductPriceException();
        }
    }
}
