package com.sdr.warehouse.exception;

import lombok.Getter;

@Getter
public class NotEnoughItemsInWarehouseException extends RuntimeException {

    private final Long productId;

    public NotEnoughItemsInWarehouseException(Long productId) {
        this.productId = productId;
    }
}
