package com.sdr.warehouse.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@NoArgsConstructor
public class Transaction {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    private Client client;

    @OneToMany(mappedBy = "transaction", cascade = ALL)
    @JsonManagedReference
    private List<TransactionProduct> products;

    private Transaction(Client client) {
        this.client = client;
    }

    public static Transaction of(Client client) {
        return new Transaction(client);
    }
}
