package com.sdr.warehouse.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@NoArgsConstructor
public class TransactionProduct {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @ManyToOne
    @JsonBackReference
    private Transaction transaction;

    @ManyToOne
    private WarehouseProduct warehouseProduct;

    @Column
    private Integer numberOfItems;

    private TransactionProduct(Transaction transaction, WarehouseProduct warehouseProduct, Integer numberOfItems) {
        this.transaction = transaction;
        this.warehouseProduct = warehouseProduct;
        this.numberOfItems = numberOfItems;
    }

    public static TransactionProduct of(Transaction transaction, WarehouseProduct warehouseProduct, Integer numberOfItems) {
        return new TransactionProduct(transaction, warehouseProduct, numberOfItems);
    }
}
