package com.sdr.warehouse.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@NoArgsConstructor
public class WarehouseProduct {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @OneToOne(optional = false)
    private Product product;

    @Column(nullable = false)
    private Integer numberOfItems;

    private WarehouseProduct(Product product, Integer numberOfItems) {
        this.product = product;
        this.numberOfItems = numberOfItems;
    }

    public static WarehouseProduct of(Product product, Integer numberOfItems) {
        return new WarehouseProduct(product, numberOfItems);
    }
}
