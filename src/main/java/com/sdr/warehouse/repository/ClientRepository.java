package com.sdr.warehouse.repository;

import com.sdr.warehouse.entity.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {
}
