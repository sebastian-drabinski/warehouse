package com.sdr.warehouse.repository;

import com.sdr.warehouse.entity.Client;
import com.sdr.warehouse.entity.Transaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    List<Transaction> findByClient(Client client);

    @Query("SELECT DISTINCT t FROM Transaction t " +
            "LEFT JOIN t.products tp LEFT JOIN tp.warehouseProduct wp LEFT JOIN wp.product p " +
            "WHERE p.id = :productId")
    List<Transaction> findByProduct(@Param("productId") Long productId);
}
