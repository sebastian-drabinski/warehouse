package com.sdr.warehouse.repository;

import com.sdr.warehouse.entity.WarehouseProduct;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface WarehouseRepository extends CrudRepository<WarehouseProduct, Long> {

    @Query("SELECT wp FROM WarehouseProduct wp LEFT JOIN wp.product p WHERE p.id = :productId")
    Optional<WarehouseProduct> findByProductId(@Param("productId") Long productId);
}
