package com.sdr.warehouse.dto;

import lombok.Data;

@Data
public class TransactionProductDto {

    private Long warehouseProductId;
    private Integer numberOfItems;
}
