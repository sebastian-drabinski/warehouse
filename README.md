# Warehouse

Aplikacja wspomagająca pracę hurtowni

## Użyte technologie

* Java 11
* Spring Boot 2.6.4
* Maven
* Baza danych H2 (in memory)
* Lombok
* Spring Data
* REST
* Swagger UI

## Uruchomienie aplikacji

Wymagana jest Java11.

W katalogu głównym projektu dołączony jest już zbudowany plik JAR (`warehouse.jar`). Wystarczy go uruchomić poniższym
poleceniem.

`java -jar warehouse.jar`

## Budowanie projektu

Projekt można zbudować Mavenem używając dołączonego skryptu (maven-wrapper), wykonując jedno z poniższych poleceń:

* `./mvnw clean install` w systemie Linux,
* `./mvnw.cmd clean install` w systemie Windows.

Wymagane do tego jest ustawienie lokalizacji JDK11 w zmiennej środowiskowej `JAVA_HOME`.

## Lista usług (REST API)

Swagger UI -> http://localhost:8080/swagger-ui.html

### Kontrolery udostępniające wymagane endpointy

* `client-controller` — dodawanie i wyświetlanie klientów
* `product-controller` — dodawanie i wyświetlanie produktów
* `warehouse-controller` — dodawanie i wyświetlanie stanów magazynowych produktów
* `transaction-controller` — tworzenie transakcji (sprzedaż) i wyświetlanie historii transakcji dla klienta lub produktu

## Baza danych (H2 in memory)

Konsola (GUI) -> http://localhost:8080/h2-console

Należy skopiować parametry połączenia z pliku `application.properties` jak na poniższym zrzucie ekranu.

login/hasło: admin/admin

![](h2-console.png)
